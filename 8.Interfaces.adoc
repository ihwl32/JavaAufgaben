= Interfaces
Dominikus Herzberg, Christopher Schölzel
:toc: left
:toctitle: Inhaltsverzeichnis
:toclevels: 4
:icons: font
:stylesheet: italian-pop.asciidoc.css
:source-highlighter: coderay
:sourcedir: code/8

include::prelude.adoc[]

Die Aufgaben in diesem Kapitel üben den Umgang mit Interfaces, auch Schnittstellen genannt, ein.

.Mehr ist besser
****
Uns fehlen definitiv noch ein paar gute Aufgaben. Wenn Sie mögen, machen Sie uns Vorschläge!
****

== UPN-Taschenrechner

CAUTION: Für diese Aufgabe sollten Sie sich bereits mit Exceptions beschäftigt haben. Wenn Sie sich schon einmal an einer Umsetzung von Stapeln (einer Datenstruktur der Informatik) versucht haben, so ist das auch von Vorteil.

Die Firma Hewlett Packard (HP) stellte seinerzeit Taschenrechner her, die eine Besonderheit auszeichnete: ihnen fehlte die `=`-Taste ebenso wie die Klammer-Tasten `(` und `)`; siehe z.B. diesen https://en.wikipedia.org/wiki/HP_calculators[Wikipedia-Beitrag].

Diese Taschenrechner waren bei Ingenieuren äußerst beliebt, die HP-Taschenrechner arbeiten mit der umgekehrten polnischen Notation (https://de.wikipedia.org/wiki/Umgekehrte_polnische_Notation[UPN]). Statt `1 + 2` heißt es in der UPN `1 2 +`; der Operator folgt den Operanden. Die Zahlen werden auf einem sogenannten Stapel gespeichert.  Eine Operation wie die Addition erwartet zwei Zahlen auf dem Stapel, entfernt sie und legt das Ergebnis der Addition beider Zahlen wieder auf dem Stapel ab.

So umständlich diese Arbeitsweise klingt, man bekommt stets die Zwischenergebnisse einer Rechnung zu sehen. Da Ingenieure darin geübt sind, ihre Rechenergebnisse im Kopf zu überschlagen, bietet ein UPN-Rechner eine gute Möglichkeit mitzuverfolgen, ob die Zwischenergebnisse Sinn machen und die Rechnung stimmig ist.

=== 1. Teil: Implementierung

In dieser Aufgabe sollen Sie einen einfachen stapelbasierten Taschenrechner bauen. Bilden Sie folgende Interaktion nach, beachten Sie auch die Darstellung eines Stapels mittels der `toString`-Methode.

----
jshell> new Stapel()
$5 ==> [ ]

jshell> $5.push(3).push(4)
$6 ==> [ 3 4 ]

jshell> $6.top()
$7 ==> 4

jshell> $6.sub()
$8 ==> [ -1 ]

jshell> $8.drop().isEmpty()
$9 ==> true
----

Übrigens, der https://de.wikipedia.org/wiki/Stapelspeicher[Stapel] ist eine wichtige Datenstruktur in der Informatik. Darum liefert ihn Java in Form der Klasse `Stack` auch gleich mit. In dieser Aufgabe sollen Sie jedoch selbst einen Rechenstapel umsetzen und nicht auf `Stack` zurückgreifen.

Nennen Sie Ihre Programmdatei `UpnCalc.java` -- das ist wichtig für den zweiten Teil der Aufgabe.

Folgendes Interface gilt es zu implementieren:

----
interface UPNRechner {
    Stapel push(Integer item);
    Stapel drop();
    Integer top();
    Stapel add();
    Stapel sub();
    Boolean isEmpty();
}
----

Die Methode `drop()` soll eine `IllegalArgumentException` mit dem Hinweis `Stack Underflow` werfen, wenn man versucht, vom leeren Stapel mit `drop` ein Element zu entfernen. Gleiches gilt für die Methode `top()`. Nutzen Sie diese Methoden, wenn Sie `add()` (Addition) implementieren. Die Methode `sub` (Subtraktion) soll sich der Methode `add()` bedienen.

.Was sind Interfaces?
****
Interfaces stellen eine Reihe von Methodenköpfen zusammen, deren Implementierung man per `implements` im Kopf einer Klassendeklaration einfordern kann. Die Klasse muss dann jeden dieser Methodenköpfe aufführen und dazu einen Methodenrumpf deklarieren.
****

CAUTION: Die Nutzung von Interfaces zusammen mit der JShell bringt im Moment (build 9-ea+146) noch Probleme mit sich. Ein `\reset` vom dem erneuten Laden Ihres Programms kann helfen. Im Zweifel kommentieren Sie das Interface aus.

include::preDetailsSolution.adoc[]
Im Code gibt es einen kleinen Vorgeschmack auf den Einsatz von Lambda-Ausdrücken, siehe Methode `op` und `mul`.
[source,java]
----
include::{sourcedir}\UpnCalc.java[]
----
include::postDetails.adoc[]

=== 2. Teil: Ohne JShell laufen lassen

Man kann Java-Programme selbstverständlich auch ohne die JShell laufen lassen. Probieren Sie das einmal aus. Dafür ergänzen Sie in Ihrem Programm folgenden Programmcode:

----
class UpnCalc {
    public static void main(String[] args) {
        System.out.println(new Stapel());
        System.out.println(new Stapel().push(3));
        System.out.println(new Stapel().push(3).push(4).mul());
    }
}
----

Achten Sie darauf, dass Ihre Programmdatei wirklich `UpnCalc.java` heißt. Mit dem Programm `javac` übersetzen Sie Ihr Java-Programm übersetzen, mit `java` ausführen Sie es aus. Finden Sie heraus, wie genau der Compiler (`javac`) und der ByteCode-Interpreter (`java`) der Java Virtual Machine (JVM) aufzurufen sind.

== Comparable

Die Methode `sort(Object[])` der Klasse `java.util.Arrays` sortiert beliebige Objekte anhand ihrer _natürlichen Ordnung_. Für selbstgeschriebene Klassen kann man diese natürliche Ordnung definieren, indem man das Interface `Comparable` implementiert.

Ergänzen Sie den unten stehenden Code, so dass Sie ein Array von `Point`-Objekten mittels `Arrays.sort` sortieren können. Die Sortierung soll aufsteigend nach der x-Koordinate erfolgen. Bei gleicher x-Koordinate soll entsprechend nach aufsteigender y-Koordinate sortiert werden.

----
class Point implements Comparable<Point> {  //<1>
  int x;
  int y;
  Point(int x, int y) {
    this.x = x;
    this.y = y;
  }
}
----
<1> Der Teil in spitzen Klammern stellt das Argument für den Typparameter `T` in der Definition des Interfaces `Comparable` dar. Solche Typparameter erlauben es, Klassen für beliebige Typen zu implementieren. `Comparable<Point>` heißt in diesem Fall also "vergleichbar mit Objekten vom Typ Point" genauso wie `Comparable<Car>` heißen würde "vergleichbar mit Objekten vom Typ Car". Ohne Typparameter müsste man zwei separate Interfaces `ComparableToPoint` und `ComparableToCar` erstellen.

include::preDetailsSolution.adoc[]
[source, java]
----
include::{sourcedir}\comparablePoint.java[]
----
<1> Die Methode `toString` ist zwar nicht Teil der Aufgabenstellung, hilft aber enorm, um den Inhalt des Arrays zu inspizieren mit `Arrays.toString`.
<2> Das Interface `Comparable` definiert die Methode `compareTo`. Dadurch dass unsere Klasse das Interface implementiert, muss sie auch diese Methode mit der gleichen Signatur und Sichtbarkeit implementieren.
include::postDetails.adoc[]
