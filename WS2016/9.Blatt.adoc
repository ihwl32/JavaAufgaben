= 9. Woche: OO-Aufgaben
Dominikus Herzberg, Nadja Krümmel, Christopher Schölzel
V1.0, 2016-12-09
:toc:
:icons: font
:solution:
:source-highlighter: coderay
:sourcedir: ./code/9
:imagedir: ./images

// Topics: https://git.thm.de/dhzb87/oop/issues/????

== Dateien

=== Playlist mischen

Ein sehr einfaches Format für MP3-Playlists ist das https://en.wikipedia.org/wiki/M3U[M3U]-Format. M3U-Dateien bestehen einfach nur aus Dateinamen die durch Zeilenumbrüche getrennt sind.

Schreiben Sie ein kleines Java-Programm, das eine solche Playlist zufällig durchmischt.

[TIP]
====
Die einfachste Möglichkeit, eine Textdatei zu lesen oder zu schreiben, ist die Klasse `java.noi.files.Files`. Dort gibt es die Methoden `newBufferedReader` bzw. `newBufferedWriter`, die Objekte vom Typ `BufferedReader` bzw. `BufferedWriter` erzeugen. Mit diesen Objekten können sie sehr bequem zeilenweise lesen und schreiben.
====

[WARNING]
====
Passen Sie auf, auf welche Dateien sie ihr Programm loslassen. In einem Stück Java-Code oder einer Notiz-Datei kann es für ziemlich viel Chaos sorgen. ;)
====

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/shufflePlaylist.java[]
----
<1> Diese generische Methode funktioniert unabhängig von dem Elementtyp der Liste. Es wäre auch eine korrekte Lösung gewesen, die Methode nur für Listen vom Typ `String` zu schreiben und den Typparameter `<E>` vor dem Methodenkopf wegzulassen.
endif::solution[]

=== Playlist sortieren

In M3U-Dateien findet man oft auch die gängige Erweiterung des _extended M3U_ (zu erkennen an dem Header `#EXTM3U` in der ersten Zeile.). Dieses Format erlaubt es, zusätzliche Metainformationen zu einer Datei zu speichern.

Schreiben Sie ein Programm, das eine Playlist nach Künstler und Titel sortieren kann.

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/sortPlaylist.java[]
----
<1> Mit der Methode `group` lassen sich sehr angenehm alle Teile eines regulären Ausdrucks, die in runden Gruppenklammern stehen auslesen. Gruppe 0 ist dabei immer der gesamte Match. Wenn man einmal möchte, dass eine Gruppenklammer keine solche _capture group_ erzeugt, kann man ein `?:` direkt vor die öffnende Klammer schreiben.
endif::solution[]

[TIP]
====
Vorgefertigte Sortieralgorithmen finden Sie in den Klassen `java.util.Collections` bzw. `java.util.Arrays`.
====

=== Transponieren

Schreiben Sie ein Programm das eine Textdatei im CSV-Format (Comma (bzw. Character) Separated Values) einliest und eine neue CSV-Datei erzeugt in der Spalten und Zeilen vertauscht sind.

Beispieleingabe:
----
Vorname;Nachname;Punkte
Berta;Besserwisser;100
Karl;Knüppel;70
Rudi;Rasenmäher;80
----

Beispielausgabe:
----
Vorname;Berta;Karl;Rudi
Nachname;Besserwisser;Knüppel;Rasenmäher
Punkte;100;70;80
----

[TIP]
====
Machen Sie nicht zu viel auf einmal. Bei dieser Aufgabe bietet es sich an erst einmal eine Methode zu schreiben, die CSV-Dateien in eine geeignete Datenstruktur einliest und eine entsprechende Methode, die wieder CSV-Dateien schreiben kann. Nutzen Sie dazu die Hilfsmethoden der Klasse `java.nio.files.Files`.
====

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/transpose.java[]
----
endif::solution[]


== Reguläre Ausdrücke

=== E-Mail-Adressen

Schreiben Sie eine Klasse `EMail`, die eine E-Mail-Adresse darstellt. Der Konstruktor der Klasse soll die Adresse als String übernehmen und in ihre Bestandteile zerlegen, so dass ihre Klasse dem folgenden Interface entspricht:

[TIP]
====
Zum Testen von regulären Ausdrücken helfen Onlinetools wie https://regex101.com[regular expressions 101] enorm.
====

[source,java]
----
include::{sourcedir}/email.java[]
----

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/email_solution.java[]
----
endif::solution[]

Wenn der übergebene String nicht den Spezifikationen entspricht, die im Interface angegeben sind, soll der Konstruktor eine `IllegalArgumentException` werfen.

=== Parsen von Webseiten

Speichern Sie sich das https://moodle.thm.de/mod/forum/view.php?id=142571[Nachrichtenforum] des OOP-Kurses als HTML-Datei ab. Laden Sie diese HTML-Datei in einem Java-Programm und geben Sie nur die Titel der Mitteilungen untereinander auf der Konsole aus.

== Algorithmen

=== Caesar-Verschlüsselung

Implementieren Sie eine einfache https://en.wikipedia.org/wiki/Caesar_cipher[Caesar-Verschlüsselung] indem Sie einen übergebenen String erst in Kleinbuchstaben umwandeln und dann alle darin enthaltenen Buchstaben von 'a' bis 'z' um eine vorgegebene Schrittzahl im Alphabet verschieben. Wenn ein Buchstabe dabei über das 'z' hinaus verschoben werden müsste, beginnt man wieder bei 'a'.

== BigInteger und BigDecimal

=== Hoher PowerTower

Schreiben Sie die Methode `PowerTower` aus Blatt 4 noch einmal neu unter Verwendung der Klasse `java.math.BigInteger`.

=== Fakultät

Schreiben Sie ein Kommandozeilen-Anwendung, die die Fakultät einer Zahl berechnet.

.Beispielhafte Interaktion
----
> java Fact 7
5040
----

* Die Berechnung der Fakultät (_factorial_) soll rekursiv erfolgen.
* Verwenden Sie zur Berechnung `BigInteger`, damit mit beliebig großen Ganzzahlen gerechnet werden kann
* Behandeln Sie Fehleingaben des Anwenders bzw. der Anwenderin auf eine geeignete Weise
* Einen erfolgreichen Programmabschluß beenden Sie mit `System.exit(0)`; das meldet anderen Konsolenprogrammen einen erfolgreichen Abschluss der Berechnung
* Bei Fehleingaben beenden Sie das Programm nach einer geeigneten Fehlermeldung mit `System.exit(1)`; das meldet anderen Konsolenprogrammen einen Abbruch

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/Fact.java[]
----
endif::solution[]




////
Ideen (CS):
- Dateien
  -- CSV-Dateien
    --- Transponieren (Spalten <-> Zeilen)
    --- Mittelwert aller Spalten bilden, falls diese Zahlen enthalten
      ---- Zusatzaufgabe: Fehldende oder illegale Spalten ignorieren
  -- M3U8-Playlists
    --- Playlist mischen
      ---- Zusatzaufgabe: extended M3U, file header und EXTINF behalten beim mischen
    --- Playlist sortieren nach EXTINF-Info
  -- PGM-Bildateien
- Regex
  -- email erkennen und splitten
  -- pretty-printed datei lesen (beliebig viele spaces zwischen elementen)
  -- Nachrichtenforum als Datei speichern (https://moodle.thm.de/mod/forum/view.php?f=7531) und aus inhalt nur Titel und Datum der nachrichten extrahieren
- Algorithmen
  -- Caesar-Verschlüsselung
  -- Vigenere-Verschlüsselung
  -- binäre Suche
  -- Sierpinski-Dreieck
- BigInteger
  -- PowerTower mit BigInt
  -- Fakultät mit BigInt
- Zeit und Datum
  -- ???
- Collection und Stream-Interfaces
  -- ???
////
