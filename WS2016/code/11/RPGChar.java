class RPGChar {
  String name;
  int strength;
  int hitpoints;
  RPGChar(String name, int strength, int hitpoints) {
    this.name = name;
    this.strength = strength;
    this.hitpoints = hitpoints;
  }
  void attack(RPGChar other) {
    other.hitpoints -= strength;
  }
  void heal() {
    hitpoints += 10;
    if (hitpoints > 100) {
      hitpoints = 100;
    }
  }
}
