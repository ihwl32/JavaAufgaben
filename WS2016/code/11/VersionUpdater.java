import java.util.regex.*;
import java.io.*;
import java.nio.file.*;
import java.nio.*;
import java.nio.charset.*;

public class VersionUpdater {
  public static void main(String[] args) {
    Pattern p = Pattern.compile("Version (\\d+)\\.");
    try (
      BufferedReader br = Files.newBufferedReader(
        Paths.get(args[0]), StandardCharsets.UTF_8
      )
    ) {
      String line;
      while((line = br.readLine()) != null) {
        Matcher m = p.matcher(line);
        if(m.find()) {
          int s = m.start();
          int e = m.end();
          int v = Integer.parseInt(m.group(1));
          System.out.print(line.substring(0, s));
          System.out.printf("Version %d.", v + 1);
          System.out.println(line.substring(e, line.length()));
        } else {
          System.out.println(line);
        }
      }
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
  }
}
