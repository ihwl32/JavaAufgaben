public class ArrayIterator<E> implements Iterator<E> {
  private E[] ar;
  private int idx;
  public ArrayIterator(E[] ar) {
    this.ar = ar;
  }
  public boolean hasNext() {
    return idx < ar.length;
  }
  public E next() {
    return ar[idx++];
  }
  public void remove() {
    throw new UnsupportedOperationException();
  }
}
