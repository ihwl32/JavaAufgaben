<E> Map<E, Integer> count(Collection<E> coll) {
  Map<E, Integer> res = new HashMap<>();
  for(E el: coll) {
    Integer oldCount = res.get(el);
    if(oldCount == null) oldCount = 0;
    res.put(el, oldCount + 1);
  }
  return res;
}
