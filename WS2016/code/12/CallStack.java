class CountEverything {
  int rekCounter = 0;
  int itCounter = 0;
  int rekStackCounter = 0;
  int itStackCounter = 0;
  int rekStackMax = 0;
  int itStackMax = 0;
  int fakRek(int n) {
    rekCounter++;
    rekStackCounter++;
    if (rekStackCounter > rekStackMax) {
      rekStackMax = rekStackCounter;
    }
    int res;
    if (n < 1) res = 1;
    else res = n * fakRek(n-1);
    rekStackCounter--;
    return res;
  }
  int fakIt(int n) {
    itCounter++;
    itStackCounter++;
    if (itStackCounter > itStackMax) {
      itStackMax = itStackCounter;
    }
    int res = 1;
    for(int i = 1; i <= n; i++) res *= i;
    itStackCounter--;
    return res;
  }
}

CountEverything ce = new CountEverything();
printf("fr = %d\n",ce.fakRek(3));
printf("fi = %d\n",ce.fakIt(3));
printf("rc = %d\n",ce.rekCounter);
printf("ic = %d\n",ce.itCounter);
printf("rs = %d\n",ce.rekStackCounter);
printf("is = %d\n",ce.itStackCounter);
printf("rm = %d\n",ce.rekStackMax);
printf("sm = %d\n",ce.itStackMax);
