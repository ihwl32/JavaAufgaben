class SwappableList1<E> implements Swappable {
  List<E> lst;
  SwappableList1(List<E> lst) { this.lst = lst; }
  public void swap(int i, int j) {
    E tmp = lst.get(i);
    lst.set(i, lst.get(j));
    lst.set(j, tmp);
  }
  public int size() {
    return lst.size();
  }
}

SwappableList1<Integer> lst;
Integer[] ar = {1,2,3,4,5,6};
lst = new SwappableList1<Integer>(Arrays.asList(ar));
shuffle(lst);
printf(lst.lst.toString());
