class Stack {
  int value;
  Stack rest;
  Stack(int value, Stack rest) {
    this.value = value;
    this.rest = rest;
  }
  Stack(int value) {
    this(value, null);
  }
}