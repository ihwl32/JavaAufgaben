import java.net.URL;
import java.io.InputStream;
import java.nio.charset.*; // Charset, StandardCharsets

String streamToString(InputStream is, Charset cs) throws IOException {
  InputStreamReader isr = new InputStreamReader(is, cs);
  StringBuilder sb = new StringBuilder();
  int c = 0;
  while((c = isr.read()) >= 0) sb.append((char) c);
  return sb.toString();
}

URL url = new URL("http://api.steampowered.com/ISteamWebAPIUtil/GetSupportedAPIList/v0001");
String content;
try(InputStream is = url.openStream()) {                    // <1>
  content = streamToString(is, StandardCharsets.UTF_8);
}
printf(content);
