import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.lang.reflect.Array;

String[][] readCSV(String inFile) throws IOException {
  Path inPath = Paths.get(inFile);
  Charset utf8 = StandardCharsets.UTF_8;
  List<String[]> data = new ArrayList<>();
  try(BufferedReader br = Files.newBufferedReader(inPath, utf8)) {
    String line;
    while ((line = br.readLine()) != null) {
      if(line.trim().length() > 0) {
        data.add(line.split(";"));
      }
    }
  }
  return data.toArray(new String[0][]);
}

void writeCSV(String[][] data, String outFile) throws IOException {
  Path outPath = Paths.get(outFile);
  Charset utf8 = StandardCharsets.UTF_8;
  try(BufferedWriter bw = Files.newBufferedWriter(outPath, utf8)) {
    for(int i = 0; i < data.length; i++) {
      for(int j = 0; j < data[i].length; j++) {
        if(j > 0) bw.write(";");
        bw.write(data[i][j]);
      }
      bw.write("\n");
    }
  }
}

<T> T[][] transpose(T[][] data) {                                          // <1>
  // String[][] transposed = new String[data[0].length][data.length];
  @SuppressWarnings("unchecked")                                           // <3>
  T[][] transposed = (T[][]) Array.newInstance(                            // <2>
    data[0][0].getClass(),
    data[0].length,
    data.length
  );
  for(int i = 0; i < data.length; i++) {
    for(int j = 0; j < data[i].length; j++) {
      transposed[j][i] = data[i][j];
    }
  }
  return transposed;
}

void transposeCSV(String inFile, String outFile) throws IOException {
  String[][] data = readCSV(inFile);
  data = transpose(data);
  writeCSV(data, outFile);
}