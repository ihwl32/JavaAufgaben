int[] cumsum(int[] ar) {
  int[] res = new int[ar.length];
  int sum = 0;
  for(int i = 0; i < ar.length; i++) {
    sum += ar[i];
    res[i] = sum;
  }
  return res;
}