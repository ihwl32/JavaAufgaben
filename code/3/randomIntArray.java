import java.util.Random;

int[] randomIntArray(int size, double percent) {
    int n = (int) (size * percent);
    int[] array = new int[size];
    Random r = new Random();
    while(n > 0) {
        int next = r.nextInt(size);
        if (array[next] != 0) continue;
        array[next] = 1;
        n--;
    }
    return array;
}

int testCount = IntStream.rangeClosed(1,10).map(n -> IntStream.of(randomIntArray(10,0.8)).sum()).sum()
if (testCount != 80) throw new AssertionError();
