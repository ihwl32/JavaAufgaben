class Unit {
  int number;
  String type;
  int damage;
  int hitpoints;
  Unit(String type, int damage, int hitpoints, int number) {
    this.type = type;
    this.damage = damage;
    this.hitpoints = hitpoints;
    this.number = number;
  }
  static Unit createBoxers(int n) {
    return new Unit("Boxer", 1, 1, n);
  }
  static Unit createPikemen(int n) {
    return new Unit("Pikeniere", 2, 1, n);
  }
  static Unit createCatapults(int n) {
    return new Unit("Katapulte", 5, 2, n);
  }
  int killCount(Unit enemy) {
    float modifiedDamage = damage * multiplierAgainst(enemy);
    float kcRaw = number * modifiedDamage / enemy.hitpoints;
    return Math.min(enemy.number, (int) kcRaw);
  }
  float multiplierAgainst(Unit enemy) {
    float mul = 1;
    if("Katapulte".equals(type) && !"Katapulte".equals(enemy.type)) {
      mul = 3;
    } else if ("Pikeniere".equals(type) && "Boxer".equals(enemy.type)) {
      mul = 2;
    } else if ("Boxer".equals(type) && "Katapulte".equals(enemy.type)) {
      mul = 0.5f;
    }
    return mul;
  }
  String toString(String color) {
    return number + " " + color + " " + type;
  }
}

// der restliche Code bleibt unberührt