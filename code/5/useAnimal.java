
Animal a0 = new Animal("Alma", "Kuh", "muh" , 5);
Animal a1 = new Animal("Elsa", "Kuh", "muh" , 10);
Animal a2 = new Animal("Lulu", "Schildkröte", "klap" , 100);
Animal a3 = new Animal("Dori", "Fisch", "blubb" , 5);


Animal[] a = {a0, a1, a2, a3};

for (int i = 0; i < a.length; i++){
  if(a[i] != null){
    System.out.printf ("%s \n", a[i].getIdentity());
  }
}

for (int i = 0; i < a.length; i++){
  if(a[i] != null){
    System.out.printf ("%s \n", a[i].makeSound(i+1));
  }
}


System.out.printf ("Zusammen sind wir %d Jahre alt.", Animal.countAllAges(a));
