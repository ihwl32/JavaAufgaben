class Thing {
    static private Collection<Thing> inventory = new ArrayList<Thing>();
    static private Set<Integer> IDs = new HashSet<>();

    private final int ID;
    private double price = Double.NaN;
    
    Thing(int identity) {
        if (Thing.IDs.contains(identity))
            throw new IllegalArgumentException("unique code required!");
        ID = identity;
        IDs.add(identity);
        inventory.add(this);
    }

    void setPrice(double price) {
        if (price < 0.0)
            throw new IllegalArgumentException("price must be >= 0.0");
        this.price = price;    
    }

    double getPrice() { return price; }

    static Thing find(int identity) {
        for(Thing item: Thing.inventory)
            if (item.ID == identity) return item;
        return null;
    }

    static void showInventory() {
        for(Thing item: Thing.inventory)
            System.out.printf("Item %s costs %f\n",item.ID,item.price);
    }
}

Thing t1 = new Thing(123);
Thing.showInventory();
new Thing(124);
Thing.find(124).setPrice(18.99);
Thing.showInventory();

