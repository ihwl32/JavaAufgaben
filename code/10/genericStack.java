class Stakk<E> {
  E value;
  Stakk<E> rest;
  Stakk(E value, Stakk<E> rest) {
    this.value = value;
    this.rest = rest;
  }
  Stakk(E value) {
    this(value, null);
  }
  public String toString() {
    if(rest == null) {
      return value.toString();
    } else {
      return value + "\n" + rest.toString();
    }
  }
  int size() {
    if(rest == null) return 1;
    return 1 + rest.size();
  }
  void push(E val) {
    rest = new Stakk<E>(value, rest);
    value = val;
  }
  E pop() {
    E top = value;
    value = rest.value;
    rest = rest.rest;
    return top;
  }
  E[] toArray(E[] base) {                   // <1>
    E[] ar = Arrays.copyOf(base, size());
    int i = 0;
    Stakk<E> cur;
    for(cur = this; cur.rest != null ; cur = cur.rest) {
      ar[i++] = cur.value;
    }
    ar[i] = cur.value;
    return ar;
  }
  static <E> Stakk fromArray(E[] ar) {      // <2>
    Stakk<E> s = null;
    for(int i = ar.length-1; i >= 0; i--) {
      s = new Stakk<E>(ar[i], s);
    }
    return s;
  }
}
