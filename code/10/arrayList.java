public class ArrayList<E> [...] {           // <1>
  private transient Object[] elementData;   // <3>
  private int size;
  
  [...]
  
  public E get(int index) {                 // <2>
    rangeCheck(index);
    return elementData(index);
  }
  
  [...]
  
  public boolean add(E e) {                 // <2>
    ensureCapacityInternal(size + 1);
    elementData[size++] = e;
    return true;
  }
  
  [...]
}