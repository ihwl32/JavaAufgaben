= Installation von Java 9
Dominikus Herzberg
:toc: left
:toctitle: Inhaltsverzeichnis
:toclevels: 4
:icons: font
:imagedir: images/Installation
:stylesheet: italian-pop.asciidoc.css

include::prelude.adoc[]

== Download des JDKs

Um Programme für Java 9 entwickeln zu können, benötigen Sie das _Java Development Kit_ (JDK) in der aktuellen Fassung. Wählen Sie für die Standard-Edition (SE) den Link zum Download des JDKs aus:

http://www.oracle.com/technetwork/java/javase/downloads/index.html

Suchen Sie das entsprechende Paket für Ihr Betriebssystem aus. Wichtig ist, das _Java Development Kit_ (JDK) auszuwählen, nicht das _Java Runtime Environment_ (JRE). Denn wir brauchen all die Werkzeuge und Hilfsmittel, die notwendig sind, um Java-Programme zu entwickeln. Das JRE, die Java-Laufzeit-Umgebung, ist nur dazu da, um fertig entwickelte Programme ablaufen lassen zu können. Mit dem JDK wird die JRE automatisch dazu installiert.

== Installation unter Windows

Nach dem Herunterladen der `.exe`-Datei wird die Datei durch einen Doppelklick gestartet und das JDK wird installiert. Ich empfehle, das vorgeschlagene Installationsverzeichnis beizubehalten. Ist die Installation beendet, findet man das JDK und die JRE für Java 9 im Verzeichnis `C:\Program Files\Java` -- sofern man nichts an den Grundeinstellungen verändert hat.

Da wir die ganzen Java-Entwicklungswerkzeuge über die Kommandozeile von Windows aufrufen werden, müssen wir Windows darüber informieren, wo die JDK-Programme zu finden sind. Geben Sie in der Suchzeile (meist links unten, dort wo Cortana Kontakt aufnehmen möchte) den Begriff "Umgebungsvariablen" ein, und wählen Sie dann den angebotenen Punkt "Systemumgebungsvariablen bearbeiten" aus. Ein Fenster zu den Systemeigenschaften öffnet sich. Rechts unten findet sich der Auswahlpunkt "Umgebungsvariablen...", den Sie auswählen.

image::{imageDir}/Systemeigenschaften.png[caption="Bild: ", title="Fenster mit Systemeigenschaften"]

Legen Sie eine neue Benutzervariable `JDK9` an, der Sie als Wert das Verzeichnis mit den Entwicklungsprogrammen angeben: `C:\Program Files\Java\jdk-9\bin`.

image::{imageDir}/JDK9-Variable.png[caption="Bild: ", title="Lege die Umgebungsvariable `JDK9` an"]

Die Variable `PATH` (sie gibt es in der Regel schon) muss nun bearbeitet werden; bei ihr ist als neuer Wert `%JDK9%` einzutragen. Schließen Sie die Fenster jeweils mit der Anwahl auf das OK-Feld.

image::{imageDir}/PATH-Variable.png[caption="Bild: ", title="Ergänze Zugriff auf JDK9 in der Umgebungsvariable `PATH`"]

Möchten Sie, dass auch andere Benutzer auf Ihrem Rechner mit Java arbeiten können sollen, dann müssen Sie diese zwei Arbeitsschritte statt bei den Benutzervariablen unter den Systemvariablen vornehmen.

Testen wir kurz, ob Java nun auf Ihrem Rechner läuft. Geben Sie im Suchfeld von Windows `cmd` (_command_) ein und schließen Sie die Eingabe mit einem "Enter" ab. Ein Fenster öffnet sich, die Kommandozeile ist zu sehen. Geben Sie `javac -version` ein, und Sie sollten Folgendes in dem Fenster sehen:

----
C:\Users\Dominikus>javac -version
javac 9-ea
----

image::{imageDir}/JavacVersion.png[caption="Bild: ", title="Java 9 ist erfolgreich installiert"]

Sollte der Aufruf von `javac -version` nicht erfolgreich sein, hier noch zwei Tipps, die Sie versuchen können:footnote:[Mit Dank an Yasmin M. Bell]:

* Legen Sie die Umgebungsvariable `CLASSPATH` an mit dem Wert `C:\Program Files\Java\jdk-9\lib`, dort findet sich die Java-Bibliothek (_library_).

* Legen Sie die Umgebungsvariable `JAVA_HOME` an mit dem Wert des JDK-Verzeichnisses `C:\Program Files\Java\jdk-9`.

Sollte es immer noch Schwierigkeiten geben, bitten Sie einen erfahrenen Windows-Benutzer um Hilfe. Es ist verschenkte Zeit, das Problem selber orten zu wollen, wenn man sich mit Umgebungsvariablen, Verzeichnisstrukturen etc. nicht auskennt. Ein erfahrener Windows-Benutzer bringt Ihnen Java innerhalb von wenigen Minuten zum Laufen.

== Installation unter Mac OS X

Nach dem Herunterladen der `.dmg`-Datei wird die Datei durch einen Doppelklick gestartet und das JDK wird installiert.footnote:[Mit Dank für dieses Unterkapitel an Nadja Krümmel] Ich empfehle, das vorgeschlagene Installationsverzeichnis beizubehalten. Ist die Installation beendet, findet man das JDK und die JRE für Java 9 im Verzeichnis `/Library/Java/JavaVirtualMachines/jdk-9.jdk` -- sofern man nichts an den Grundeinstellungen verändert hat.

Nun müssen Sie die Umgebungsvariable `PATH` erweitern; in der Regel ist die Variable bereits vorhanden. Dazu starten Sie ein Terminal mit der `cmd`-Taste + Leertaste, Sie geben `Terminal` ein und erweitern dann die `PATH`-Variable mit der folgenden Eingabe:

----
PATH="$PATH:/Library/Java/JavaVirtualMachines/jdk-9.jdk/Contents/Home/bin"
----

Testen wir kurz, ob Java nun auf Ihrem Rechner läuft. Geben Sie auf der Kommandozeile `javac -version` ein, und Sie sollten Folgendes in dem Fenster sehen:

----
Nadjas-MacBook-Pro:repo nadjakruemmel$ javac -version
javac 9-ea
----

(Natürlich steht bei Ihnen ein anderer, entsprechender Nutzername.)

== Installation unter Linux

Wenn Sie das JDK9 erfolgreich auf Ihrem Linux-Rechner (wie z.B. Ubuntu) installiert haben, schicken Sie mir -- wenn Sie mögen -- eine schrittweise Anleitung samt Screenshots (am besten im png-Format). Ich aktualisiere dann die Anleitung gerne auch für dieses Betriebssystem. Derweil mag Ihnen vielleicht der folgende Link helfen:

https://wiki.ubuntuusers.de/Java/Installation/OpenJDK/

Wie man das JDK9 aus den Quellen von Oracle installiert, zeigt Ihnen die Anleitung von Tobias Mierzwa -- vielen Dank dafür.

=== JDK-Installation aus Oracle-Quellen

_Autor_: Tobias Mierzwa

Die `tar.gz`-Datei von http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html downloaden. Danach müsste die `tar.gz`-Datei sich in dem Download-Ordner des Benutzers befinden, wenn Sie es nicht woanders hingelegt haben. Jetzt das Terminal starten. Bei manchen Befehlen werden Sie `root`-Rechte benötigen. Entweder verwenden Sie vor den Befehlen ein `sudo` oder Sie wechseln mit einem anfänglichen `sudo su` in den Superuser-Modus.

----
root@haxx0r1337-VirtualBox:/home/haxx0r1337/# cd Download
root@haxx0r1337-VirtualBox:/home/haxx0r1337/Downloads#
----

In dem Verzeichnis liegt jetzt die `tar.gz`-Datei. Dies können Sie mit dem Befehl `ls` (für _list_) herausfinden.
Das sieht dann so aus https://i.imgur.com/kHTnxCq.png. Mit dem Befehl `tar zxvf` wird die Datei entpackt.

----
root@haxx0r1337-VirtualBox:/home/haxx0r1337/Downloads# tar zxvf jdk-9.0.1_linux-x64_bin.tar.gz
----

Den Befehl `rm` zum Löschen von jdk-9.0.1_linux-x64_bin.tar.gz, um Speicherplatz zu sparen.

----
root@haxx0r1337-VirtualBox:/home/haxx0r1337/Downloads# rm jdk-9.0.1_linux-x64_bin.tar.gz
----

Danach mit dem `mv`-Befehl in den Ordner `/opt/` zu verschieben für Softwares.

----
root@haxx0r1337-VirtualBox:/home/haxx0r1337/Downloads# mv jdk-9.0.1/ /opt/
----

Jetzt wird das `PATH` erstellt, um die JShell von überall aus aufrufen zu können. Dafür wird die `bash.bashrc` bearbeitet. Die gibt es einmal in `/etc/bash.bashrc` und zum anderen im Home-Verzeichnis des aktuellen Nutzers. Sie müssen sich also entscheiden, ob der JShell-Aufruf für alle Anwender des Rechner oder nur für einen speziellen möglich sein soll.

Mit einem Editor wie `vi` (Achtung: nur für Experten, die sich auskennen) oder z.B. `nano` kann die Datei angepasst werden.

----
root@haxx0r1337-VirtualBox:/home/haxx0r1337/Downloads# vi /etc/bash.bashrc
----

Die beiden folgenden Zeilen sind ganz unten in der Datei am Ende einzufügen:

.Ergänzung für `bash.bashrc`
----
PATH=/opt/jdk-9.0.1/bin:$PATH
export PATH
----

Das sieht dann so aus http://i.imgur.com/Yvf6gDb.png. Damit das mit dem `PATH` klappt, muss auch der Ordner mit dem `mv`-Befehl nach `/opt/` verschoben sein.

----
root@haxx0r1337-VirtualBox:/home/haxx0r1337/Downloads# cd /
root@haxx0r1337-VirtualBox:/# source ~/.profile
----

Wenn Sie das alles gemacht haben, einmal das Terminal neu starten und überprüfen, ob alles wie geplant läuft. Folgende Java-Befehle sollten funktionieren. Das sieht dann so aus http://i.imgur.com/nPzHmHF.png.

----
root@haxx0r1337-VirtualBox:/# java -version
root@haxx0r1337-VirtualBox:/# javac -version
root@haxx0r1337-VirtualBox:/# jshell -version
----

Falls es Probleme mit dem `PATH` gibt, so hilft vielleicht die Anleitung von Oracle: https://www.java.com/de/download/help/path.xml

